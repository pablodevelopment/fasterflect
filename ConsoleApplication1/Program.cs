﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fasterflect;
using System.Reflection;

namespace FasterflectSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var type = Assembly.GetExecutingAssembly().GetType("FasterflectSample.Person");
            ExecuteNormalApi(type);
            //ExecuteCacheApi(type);
            Console.ReadKey();
        }

        private static void ExecuteNormalApi(Type type)
        {
            // Person.InstanceCount should be 0 since no instance is created yet
            AssertTrue((int)type.GetFieldValue("InstanceCount") == 0);

            // Invokes the no-arg constructor
            object obj = type.CreateInstance();

            // Now, Person.InstanceCount should be 1
            Console.WriteLine(type.GetFieldValue("InstanceCount"));

            // We can bypass the constructor to change the value of Person.InstanceCount directly
            type.SetFieldValue("InstanceCount", 2);
            Console.WriteLine((int)type.GetFieldValue("InstanceCount"));

            // Let's invoke Person.IncreaseCounter() static method to increase the counter
            type.CallMethod("IncreaseInstanceCount");
            Console.WriteLine((int)type.GetFieldValue("InstanceCount"));

            // Now, let's retrieve Person.InstanceCount via the static method GetInstanceCount
            Console.WriteLine((int)type.CallMethod("GetInstanceCount"));

            // Invoke method receiving ref/out params, we need to put arguments in an array
            var arguments = new object[] { 1, 2 };
            type.CallMethod("Swap",
                // Parameter types must be set to the appropriate ref type
                new[] { typeof(int).MakeByRefType(), typeof(int).MakeByRefType() },
                arguments);
            AssertTrue(2 == (int)arguments[0]);
            AssertTrue(1 == (int)arguments[1]);

            // Now, invoke the 2-arg constructor.  We don't even have to specify parameter types
            // if we know that the arguments are not null (Fasterflect will call arg[n].GetType() internally).
            obj = type.CreateInstance(1, "Doe");

            // id and name should have been set properly
            Console.WriteLine((int)obj.GetFieldValue("id"));
            Console.WriteLine(obj.GetPropertyValue("Name").ToString());

            // Let's have the folk walk 6 miles
            obj.CallMethod("Walk", 6);

            // Double-check the current value of the milesTravelled field
            Console.WriteLine((int)obj.GetFieldValue("milesTraveled"));


        }

        //private static void ExecuteCacheApi(Type type)
        //{
        //    var range = Enumerable.Range(0, 10).ToList();

        //    // Let's cache the getter for InstanceCount
        //    StaticMemberGetter count = type.DelegateForGetStaticFieldValue("InstanceCount");

        //    // Now cache the 2-arg constructor of Person and playaround with the delegate returned
        //    int currentInstanceCount = (int)count();
        //    ConstructorInvoker ctor = type.DelegateForCreateInstance(new[] { typeof(int), typeof(string) });
        //    range.ForEach(i =>
        //    {
        //        object obj = ctor(i, "_" + i);
        //        AssertTrue(++currentInstanceCount == (int)count());
        //        AssertTrue(i == (int)obj.GetFieldValue("id"));
        //        AssertTrue("_" + i == obj.GetPropertyValue("Name").ToString());
        //    });

        //    // Getter/setter
        //    MemberSetter nameSetter = type.DelegateForSetPropertyValue("Name");
        //    MemberGetter nameGetter = type.DelegateForGetPropertyValue("Name");

        //    object person = ctor(1, "John");
        //    AssertTrue("John" == (string)nameGetter(person));
        //    nameSetter(person, "Jane");
        //    AssertTrue("Jane" == (string)nameGetter(person));

        //    // Invoke method
        //    person = type.CreateInstance();
        //    MethodInvoker walk = type.DelegateForCallMethod("Walk", new[] { typeof(int) });
        //    range.ForEach(i => walk(person, i));
        //    AssertTrue(range.Sum() == (int)person.GetFieldValue("milesTraveled"));

        //    // Map properties
        //    var ano = new { Id = 4, Name = "Doe" };
        //    var mapper = ano.GetType().DelegateForMap(type);
        //    mapper(ano, person);
        //    AssertTrue(4 == (int)person.GetPropertyValue("Id"));
        //    AssertTrue("Doe" == (string)person.GetPropertyValue("Name"));

        //}

        public static void AssertTrue(bool expression)
        {
            if (!expression)
                throw new Exception("Not true");
            Console.WriteLine("Ok!");
        }
    }
}
